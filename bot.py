import datetime
import csv
import yaml
import os
import schedule
import time
from sqlalchemy import (
    Column,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    Boolean,
    String,
    create_engine,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relationship, sessionmaker, scoped_session
from telegram.error import BadRequest
from telegram.ext import (
    CommandHandler,
    CallbackQueryHandler,
    Filters,
    MessageHandler,
    Updater,
    ConversationHandler
)
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup


Base = declarative_base()


class Registration(Base):
    __tablename__ = 'registrations'
    id = Column(Integer, primary_key=True, nullable=False)
    # Telegram UID of person who made the registration
    issued_by = Column(Integer, nullable=False)
    date = Column(Integer, ForeignKey('dates.id'), nullable=False)
    name = Column(String(128), nullable=False)
    vaccinated = Column(Boolean, nullable=False)
    timestamp = Column(DateTime, nullable=False)

class Date(Base):
    __tablename__ = 'dates'
    id = Column(Integer, primary_key=True)
    date = Column(Date, nullable=False)


class Subscription(Base):
    __tablename__ = 'subscriptions'
    id = Column(Integer, primary_key=True)


class Admin(Base):
    __tablename__ = 'admins'
    uid = Column(Integer, primary_key=True)


def is_admin(uid, session, bot, print_error=True):
    if not session.query(Admin).filter_by(uid=uid).first():
        if print_error:
            text = f"You are not registered as an admin and therefore are not allowed to use this command. If you believe this to be a mistake, please contact {MAINTAINER}."
            bot.send_message(chat_id=uid, text=text)
        return False
    return True


def is_group_chat(chat_id, bot):
    if chat_id < 0:
        # Group
        text = "Hi, I'm the bar registration bot. To avoid clutter in the chat, please send a message to me privately."
        bot.send_message(chat_id=chat_id, text=text)
        return True
    return False


def string_to_date_name(s, bot, chat_id):
    args = s.split(' ', 2)[1:]
    if len(args) == 0:
        return True
    if len(args) != 2:
        text = "Please use no arguments or at least two (date, name)."
        bot.send_message(chat_id=chat_id, text=text)
        return False
    [str_date, name] = args
    try:
        date_obj = datetime.datetime.strptime(str_date, '%Y-%m-%d').date
    except ValueError:
        text = "Please make sure your first argument is a date in ISO format (YYYY-MM-DD)."
        bot.send_message(chat_id=chat_id, text=text)
        return False
    return (str_date, name)


def string_to_date_vax_name(s, bot, chat_id):
    args = s.split(' ', 3)[1:]
    if len(args) == 0:
        return True
    if len(args) != 3:
        text = "Please use no arguments or at least three (date, vaccinated, name)."
        bot.send_message(chat_id=chat_id, text=text)
        return False
    [str_date, str_vaccinated, name] = args
    try:
        date_obj = datetime.datetime.strptime(str_date, '%Y-%m-%d').date
    except ValueError:
        text = "Please make sure your first argument is a date in ISO format (YYYY-MM-DD)."
        bot.send_message(chat_id=chat_id, text=text)
        return False
    if str_vaccinated == "true" or str_vaccinated == "True":
        vaccinated = True
    elif str_vaccinated == "false" or str_vaccinated == "False":
        vaccinated = False
    else:
        text = "Please make sure the second argument is \"true\" or \"false\" depending on whether the person is fully vaccinated or recovered."
        bot.send_message(chat_id=chat_id, text=text)
        return False
    return (str_date, vaccinated, name)


def start(update, context):
    chat_id = update.effective_chat.id
    if is_group_chat(chat_id, context.bot):
        return
    else:
        # text = "Hi, I'm the bar registration bot. Due to Corona rules, we need people to register for any event we are holding. Please use /register to get started. You can use /cancel at any time to abort the process and use /help to find out about other commands you can use."
        text = "Hi, you can use me to check the current capacity of the bar. Send /get_capacity to check how spots are free."
    context.bot.send_message(chat_id=chat_id, text=text)


def register_with_args(date, vaccinated, name, bot, chat_id):
    with DBSession() as session:
        date = session.query(Date).filter_by(date=date).first()
        if not date:
            text = "There is no available party on that date."
            bot.send_message(chat_id=chat_id, text=text)
            return ConversationHandler.END

        if MAX_UNVAX and not vaccinated:
            unvax = session.query(Registration).filter_by(date=date.id, vaccinated=False).count()
            if unvax >= MAX_UNVAX:
                text = "Unfortunately, the maximum amount of unvaccinated people has already been reached and you therefore will not be able to attend. We hope to be able to see you at an event soon."
                bot.send_message(chat_id=chat_id, text=text)
                return ConversationHandler.END

        if MAX_PARTICIPANTS:
            participants = session.query(Registration).filter_by(date=date.id).count()
            if participants >= MAX_PARTICIPANTS:
                text = "Unfortunately, the maximum amount of participants has already been reached and you therefore will not be able to attend. We hope to be able to see you at an event soon."
                bot.send_message(chat_id=chat_id, text=text)
                return ConversationHandler.END

        # For 3G rules
        if vaccinated is None:
            vaccinated = True
        registration = Registration(
            issued_by=chat_id,
            date=date.id,
            name=name,
            vaccinated=vaccinated,
            timestamp=datetime.datetime.now()
            )
        session.add(registration)
        session.commit()
    text = "Registration successful."
    bot.send_message(chat_id=chat_id, text=text)


def register_no_args(bot, chat_id) -> int:
    with DBSession() as session:
        today = datetime.date.today()
        dates = session.query(Date).filter(Date.date>=today).order_by(Date.date).all()

        if len(dates) == 0:
            text = "Currently, there are no dates entered for another bar evening yet. Make sure to check again soon, as this may change anytime."
            bot.send_message(chat_id=chat_id, text=text)
            return ConversationHandler.END

        keyboard = [InlineKeyboardButton(str(date.date), callback_data=str(date.date))\
                      for date in dates]

    # Make each row maximally ROW_SIZE long
    keyboard = [keyboard[i:i+ROW_SIZE] for i in range(0, len(keyboard), ROW_SIZE)]

    text = "Please a select a date at which you want to go to the bar. You can use /cancel at any time to stop the registration."
    bot.send_message(chat_id=chat_id, text=text,
                     reply_markup=InlineKeyboardMarkup(keyboard))
    return REGISTER_DATE


def register(update, context) -> int:
    chat_id = update.effective_chat.id

    if is_group_chat(chat_id, context.bot):
        return ConversationHandler.END

    # First should be date, second should be vaccinated and rest is name
    if MAX_UNVAX:
        ret = string_to_date_vax_name(update.effective_message.text, context.bot, chat_id)
        if not ret:
            return ConversationHandler.END
        if ret != True:
            date, vaccinated, name = ret
            register_with_args(date, vaccinated, name, context.bot, chat_id)
            return ConversationHandler.END
    else:
        ret = string_to_date_name(update.effective_message.text, context.bot, chat_id)
        if not ret:
            return ConversationHandler.END
        if ret != True:
            date, name = ret
            register_with_args(date, None, name, context.bot, chat_id)
            return ConversationHandler.END

    return register_no_args(context.bot, chat_id)


def ask_for_name(update, context) -> int:
    chat_id = update.effective_chat.id
    uid = update.effective_message.from_user.id
    query = update.callback_query

    # Queries need to be answered
    query.answer()
    # Hide the keyboard from the previous question
    context.bot.edit_message_reply_markup(chat_id=chat_id, message_id= query.message.message_id)

    text = f"You selected {query.data}."
    context.bot.send_message(chat_id=chat_id, text=text)

    # Enter the date into the dictionary for the user
    context.user_data['date'] = query.data

    text = "Please state the **full name** of the person who should be registered\."
    context.bot.send_message(chat_id=chat_id, text=text, parse_mode="MarkdownV2")
    return REGISTER_NAME


def ask_vaccinated(update, context) -> int:
    chat_id = update.effective_chat.id
    uid = update.effective_message.from_user.id

    # Enter the name into the dictionary for the user
    context.user_data['name'] = update.effective_message.text

    text = "Is this person fully vaccinated?"
    keyboard = [[
        InlineKeyboardButton("Yes", callback_data="vaccinated"),
        InlineKeyboardButton("No", callback_data="unvaccinated")
        ]]
    context.bot.send_message(chat_id=chat_id, text=text,
                             reply_markup=InlineKeyboardMarkup(keyboard))
    return REGISTER_VACCINATED


def confirm(update, context) -> int:
    chat_id = update.effective_chat.id
    query = update.callback_query

    if MAX_UNVAX:
        # Queries need to be answered
        query.answer()

        # Hide the keyboard
        context.bot.edit_message_reply_markup(chat_id=chat_id, message_id=query.message.message_id)

        context.user_data['vaccinated'] = query.data == "vaccinated"

        text = f"You would like to register {context.user_data['name']}, who is {query.data}, for {context.user_data['date']}. Is this data correct?"

    else:
        # Enter the name into the dictionary for the user
        context.user_data['name'] = update.effective_message.text
        context.user_data['vaccinated'] = True
        text = f"You would like to register {context.user_data['name']} for {context.user_data['date']}. Is this data correct?"


    keyboard = [[
        InlineKeyboardButton("Yes", callback_data="correct"),
        InlineKeyboardButton("No", callback_data="incorrect")
        ]]
    context.bot.send_message(chat_id=chat_id, text=text, reply_markup=InlineKeyboardMarkup(keyboard))
    return REGISTER_CONFIRM


def finish_registration(update, context) -> int:
    chat_id = update.effective_chat.id
    query = update.callback_query

    # Hide the keyboard
    context.bot.edit_message_reply_markup(chat_id=chat_id, message_id=query.message.message_id)

    if query.data == "incorrect":
        text = "Bummer. Start again using /register."
        context.bot.send_message(chat_id=chat_id, text=text)
        return ConversationHandler.END

    with DBSession() as session:
        date = session.query(Date).filter_by(date=context.user_data['date']).first()

        # If the user isn't vaccinated, we need to see if we are still under the limit
        if MAX_UNVAX and not context.user_data['vaccinated']:
            unvax = session.query(Registration).filter_by(date=date.id, vaccinated=False).count()
            if unvax >= MAX_UNVAX:
                text = "Unfortunately, the maximum amount of unvaccinated people has already been reached and you therefore will not be able to attend. We hope to be able to see you at an event soon."
                context.bot.send_message(chat_id=chat_id, text=text)
                return ConversationHandler.END

        if MAX_PARTICIPANTS:
            participants = session.query(Registration).filter_by(date=date.id).count()
            if participants >= MAX_PARTICIPANTS:
                text = "Unfortunately, the maximum amount of participants has already been reached and you therefore will not be able to attend. We hope to be able to see you at an event soon."
                context.bot.send_message(chat_id=chat_id, text=text)
                return ConversationHandler.END

        registration = Registration(
            issued_by=chat_id,
            date=date.id,
            name=context.user_data['name'],
            vaccinated=context.user_data['vaccinated'],
            timestamp=datetime.datetime.now()
            )
        session.add(registration)
        session.commit()

    text = "Thank you. You have been registered."
    context.bot.send_message(chat_id=chat_id, text=text)
    return ConversationHandler.END


def notify_subscribers(session, bot, issuer, dates):
    for date in dates:
        text = f"Registrations are open for a new event on {date.strftime('%b %d %Y')}! Use /register to sign up!"
        for subscriber in session.query(Subscription.id).all():
            user_id = subscriber.id
            if user_id == issuer:
                continue
            bot.send_message(text=text, chat_id=user_id)


def add_date(update, context):
    chat_id = update.effective_chat.id
    if is_group_chat(chat_id, context.bot):
        return

    with DBSession() as session:
        uid = update.effective_message.from_user.id
        chat_id = update.effective_chat.id
        if not is_admin(uid, session, context.bot):
            return

        # User is an admin
        date_args = update.message.text.split(' ')[1:]
        if len(date_args) == 0:
            text = "Missing argument. Please specify the date as an ISO date (YYYY-MM-DD)."
            context.bot.send_message(chat_id=chat_id, text=text)
            return

        dt_obj_list = []
        for date_str in set(date_args):
            try:
                dt_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d')
            except ValueError:
                text = f"Failed to parse \"{date_str}\" into a proper date. Make sure you formatted it correctly (YYYY-MM-DD). No dates were added."
                context.bot.send_message(chat_id=chat_id, text=text)
                return

            if not session.query(Date).filter_by(date=dt_obj).first():
                dt_obj_list.append(dt_obj)
            else:
                text = f"{date_str} has already been registered. Skipping."
                context.bot.send_message(chat_id=chat_id, text=text)

        for dt_obj in dt_obj_list:
            session.add(Date(date=dt_obj.date()))
        session.commit()
        notify_subscribers(session, context.bot, chat_id, dt_obj_list)

    text = f"Added {len(dt_obj_list)} date{'s' if len(dt_obj_list) != 1 else ''}."
    context.bot.send_message(chat_id=chat_id, text=text)


def cancel(update, context):
    chat_id = update.effective_chat.id
    if is_group_chat(chat_id, context.bot):
        return ConversationHandler.END
    text = "Operation cancelled."
    context.bot.send_message(chat_id=chat_id, text=text)
    return ConversationHandler.END


def get_csv(update, context):
    chat_id = update.effective_chat.id
    if is_group_chat(chat_id, context.bot):
        return

    with DBSession() as session:
        uid = update.effective_message.from_user.id
        chat_id = update.effective_chat.id
        if not is_admin(uid, session, context.bot):
            return

        # User is admin
        # Get date of party
        date_args = update.message.text.split(' ')[1:]
        if len(date_args) == 0:
            text = "Missing argument. Please specify the date as an ISO date (YYYY-MM-DD)."
            context.bot.send_message(chat_id=chat_id, text=text)
            return
        elif len(date_args) > 1:
            text = "Too many arguments. Please only enter one date."

        try:
            dt_obj = datetime.datetime.strptime(date_args[0], '%Y-%m-%d')
        except ValueError:
            text = f"Failed to parse \"{date_args[0]}\" into a proper date. Make sure you formatted it correctly (YYYY-MM-DD)."
            context.bot.send_message(chat_id=chat_id, text=text)
            return

        # Write data to file
        filename = f'{date_args[0]}.csv'
        with open(filename, 'w') as csv_file:
            if MAX_UNVAX:
                fieldnames = ['Name', 'Vaccinated', 'Signature']
            else:
                fieldnames = ['Name', 'Signature']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

            date = session.query(Date).filter_by(date=dt_obj).first()
            registrations = session.query(Registration).filter_by(date=date.id).order_by(Registration.name).all()

            if MAX_UNVAX:
                for registration in registrations:
                    writer.writerow({'Name': registration.name, 'Vaccinated': str(registration.vaccinated), 'Signature': ''})
            else:
                for registration in registrations:
                    writer.writerow({'Name': registration.name, 'Signature': ''})

        # Send file
        with open(filename, 'rb') as csv_file:
            context.bot.send_document(chat_id=chat_id, document=csv_file)

        # Delete file
        os.remove(filename)


def print_registrations(registrations, text):
    current_date = None
    count = 1
    for registration, date in registrations:
        if date.date != current_date:
            count = 1
            current_date = date.date
            text += f"\n*{date.date}*:\n"

        if MAX_UNVAX:
            text += f"{count}) {registration.name} - {'un' if not registration.vaccinated else ''}vaccinated\n"
        else:
            text += f"{count}) {registration.name}\n"
        count += 1
    return text


def my_registrations(update, context):
    chat_id = update.effective_chat.id

    if is_group_chat(chat_id, context.bot):
        return

    args = update.effective_message.text.split(' ')[1:]
    if len(args) == 0:
        my_registrations_no_date(chat_id, context.bot)
    else:
        my_registrations_date(chat_id, context.bot, args)


def my_registrations_no_date(chat_id, bot):
    with DBSession() as session:
        today = datetime.date.today()
        registrations = session.query(Registration, Date)\
                               .filter(Registration.issued_by == chat_id,
                                       Date.id == Registration.date,
                                       Date.date >= today)\
                               .order_by(Date.date)\
                               .all()

        if len(registrations) == 0:
            text = "You don't have any registrations for upcoming events."
            bot.send_message(chat_id=chat_id, text=text)
            return

        text = "You have the following upcoming registrations:\n\n"
        text = print_registrations(registrations, text)

    bot.send_message(chat_id=chat_id, text=text, parse_mode='Markdown')


def my_registrations_date(chat_id, bot, args):
    dates = []
    for date_str in set(args):
        try:
            date_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d').date
            dates.append(date_str)
        except ValueError:
            text = "Please make sure every argument is a date in ISO format (YYYY-MM-DD)."
            bot.send_message(chat_id=chat_id, text=text)
            return

    with DBSession() as session:
        today = datetime.date.today()
        registrations = session.query(Registration, Date)\
                               .filter(Registration.issued_by == chat_id,
                                       Date.id == Registration.date,
                                       Date.date.in_(dates))\
                               .order_by(Date.date)\
                               .all()

        if len(registrations) == 0:
            text = "You don't have any registrations for those dates."
            bot.send_message(chat_id=chat_id, text=text)
            return

        text = "You have the following upcoming registrations:\n\n"
        text = print_registrations(registrations, text)

    bot.send_message(chat_id=chat_id, text=text, parse_mode='Markdown')


def delete_with_args(date, name, bot, chat_id):
    with DBSession() as session:
        date = session.query(Date).filter_by(date=date).first()
        if not date:
            text = "There is no available party on that date."
            bot.send_message(chat_id=chat_id, text=text)
            return

        if is_admin(chat_id, session, bot, False):
            entry = session.query(Registration)\
                           .filter(Registration.date == Date.id,
                                   Registration.name == name)\
                           .first()
        else:
            entry = session.query(Registration)\
                           .filter(Registration.date == Date.id,
                                   Registration.name == name,
                                   Registration.issued_by == chat_id)\
                           .first()
        if not entry:
            text = "There is no such registration."
            bot.send_message(chat_id=chat_id, text=text)
            return
        session.delete(entry)
        session.commit()
    text = "Registration successfuly removed."
    bot.send_message(chat_id=chat_id, text=text)


def delete_no_args(bot, chat_id):
    with DBSession() as session:
        # Get dates at which the user is registered
        today = datetime.date.today()
        dates = session.query(Date).distinct()\
                       .filter(Date.id == Registration.date,
                               Registration.issued_by == chat_id,
                               Date.date >= today)\
                       .order_by(Date.date)\
                       .all()

        if len(dates) == 0:
            text = "You don't have any registrations for upcoming events."
            bot.send_message(chat_id=chat_id, text=text)
            return ConversationHandler.END

        # Let user select a date
        keyboard = [InlineKeyboardButton(str(date.date), callback_data=str(date.date))\
                      for date in dates]

    # Make each row maximally ROW_SIZE long
    keyboard = [keyboard[i:i+ROW_SIZE] for i in range(0, len(keyboard), ROW_SIZE)]

    text = "Please select a date for which you want to delete a registration. You can use /cancel at any time to stop the deletion."
    bot.send_message(chat_id=chat_id, text=text,
                     reply_markup=InlineKeyboardMarkup(keyboard))
    return DELETE_DATE


def delete_registration(update, context) -> int:
    chat_id = update.effective_chat.id

    if is_group_chat(chat_id, context.bot):
        return ConversationHandler.END

    ret = string_to_date_name(update.effective_message.text, context.bot, chat_id)
    if not ret:
        return ConversationHandler.END
    if ret != True:
        date, name = ret
        delete_with_args(date, name, context.bot, chat_id)
        return ConversationHandler.END

    return delete_no_args(context.bot, chat_id)


def delete_ask_for_name(update, context) -> int:
    chat_id = update.effective_chat.id
    query = update.callback_query

    # Queries need to be answered
    query.answer()
    context.user_data['date'] = query.data

    # Hide the keyboard
    context.bot.edit_message_reply_markup(chat_id=chat_id, message_id=query.message.message_id)
    context.bot.send_message(chat_id=chat_id, text=f'You selected {query.data}.')

    with DBSession() as session:
        today = datetime.date.today()
        registrations = session.query(Registration)\
                               .filter(Registration.issued_by == chat_id,
                                       Registration.date == Date.id,
                                       Date.date >= today)\
                               .order_by(Registration.name)\
                               .all()

        # Let user select a date
        keyboard = [InlineKeyboardButton(str(registration.name), callback_data=str(registration.id))\
                      for registration in registrations]

    keyboard = [keyboard[i:i+ROW_SIZE] for i in range(0, len(keyboard), ROW_SIZE)]

    text = "Please select the name of the person you want to deregister."
    context.bot.send_message(chat_id=chat_id, text=text,
                             reply_markup=InlineKeyboardMarkup(keyboard))
    return DELETE_NAME


def delete_confirm(update, context) -> int:
    chat_id = update.effective_chat.id
    query = update.callback_query

    # Queries need to be answered
    query.answer()

    # Hide the keyboard
    context.bot.edit_message_reply_markup(chat_id=chat_id, message_id=query.message.message_id)

    with DBSession() as session:
        name = session.query(Registration.name).filter_by(id=query.data).first().name
    text = f"You would like to delete the registration for {name} on {context.user_data['date']}. Is this correct?"

    keyboard = [[
        InlineKeyboardButton("Yes", callback_data=f"correct-{query.data}"),
        InlineKeyboardButton("No", callback_data="incorrect")
        ]]
    context.bot.send_message(chat_id=chat_id, text=text, reply_markup=InlineKeyboardMarkup(keyboard))

    return DELETE_CONFIRM


def finish_delete(update, context) -> int:
    chat_id = update.effective_chat.id
    query = update.callback_query

    # Queries need to be answered
    query.answer()

    # Hide the keyboard
    context.bot.edit_message_reply_markup(chat_id=chat_id, message_id=query.message.message_id)
    args = query.data.split('-')
    if args[0] == "incorrect":
        text = "Bummer. Start again using /delete_registration."
        context.bot.send_message(chat_id=chat_id, text=text)
        return ConversationHandler.END

    # Data is correct
    register_id = args[1]
    with DBSession() as session:
        session.query(Registration).filter_by(id=register_id).delete()
        session.commit()

    text = "Registration has been deleted."
    context.bot.send_message(chat_id=chat_id, text=text)

    return ConversationHandler.END


def all_registrations(update, context):
    chat_id = update.effective_chat.id

    if is_group_chat(chat_id, context.bot):
        return

    with DBSession() as session:
        uid = update.effective_message.from_user.id
        chat_id = update.effective_chat.id
        if not is_admin(uid, session, context.bot):
            return

    args = update.effective_message.text.split(' ')[1:]
    if len(args) == 0:
        all_registrations_no_date(session, chat_id, context.bot)
    else:
        all_registrations_date(session, chat_id, context.bot, args)


def all_registrations_date(session, chat_id, bot, args):
    dates = []
    for date_str in set(args):
        try:
            date_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d').date
            dates.append(date_str)
        except ValueError:
            text = "Please make sure every argument is a date in ISO format (YYYY-MM-DD)."
            bot.send_message(chat_id=chat_id, text=text)
            return

    with DBSession() as session:
        today = datetime.date.today()
        registrations = session.query(Registration, Date)\
                               .filter(Date.id == Registration.date,
                                       Date.date.in_(dates))\
                               .order_by(Date.date)\
                               .all()

        if len(registrations) == 0:
            text = "There are no registrations for those dates."
            bot.send_message(chat_id=chat_id, text=text)
            return

        text = "The following registrations have been made for upcoming events:\n"
        text = print_registrations(registrations, text)

    bot.send_message(chat_id=chat_id, text=text, parse_mode='Markdown')


def all_registrations_no_date(session, chat_id, bot):
    with DBSession() as session:
        # User is an admin
        today = datetime.date.today()
        registrations = session.query(Registration, Date)\
                               .filter(Date.id == Registration.date,
                                       Date.date >= today)\
                               .order_by(Date.date)\
                               .all()

        if len(registrations) == 0:
            text = "There are no registrations for upcoming events."
            bot.send_message(chat_id=chat_id, text=text)
            return

        text = "The following registrations have been made for upcoming events:\n"
        text = print_registrations(registrations, text)

    bot.send_message(chat_id=chat_id, text=text, parse_mode='Markdown')


def edit_subscription(subscribe, user_id, bot):
    if is_group_chat(user_id, bot):
        return

    with DBSession() as session:
        sub = session.query(Subscription).filter_by(id=user_id).first()

        # unsubscribe
        if not subscribe:
            if sub is None:
                text = "You already aren't subscribed."
            else:
                session.delete(sub)
                session.commit()
                text = "You've successfully unsubscribed."

        # subscribe
        else:
            if sub is not None:
                text = "You are already subscribed."
            else:
                session.add(Subscription(id=user_id))
                session.commit()
                text = "You've successfully subscribed. You'll be notified when the registration for a new event opens. Note that you will still have to register manually."

        bot.send_message(text=text, chat_id=user_id)


def subscribe(update, context):
    edit_subscription(True, update.effective_chat.id, context.bot)


def unsubscribe(update, context):
    edit_subscription(False, update.effective_chat.id, context.bot)


def remind_unregister():
    today = datetime.date.today()
    with DBSession() as session:
        registrations = session.query(Registration)\
                               .filter(Date.id == Registration.date,
                                       Date.date >= today)\
                               .distinct()\
                               .all()
        # Only send messages if the list is full in case of 3G
        # This also works if MAX_PARTICIPANTS is None
        if len(registrations) != MAX_PARTICIPANTS:
                return

        users = {registration.issued_by for registration in registrations}
        text = "You are registered for an upcoming event that has reached the maximum amount of participants. If you can't make it, please use /delete_registration to deregister. Thank you."
        for user_id in users:
            updater.bot.send_message(text=text, chat_id=user_id, disable_notification=True)


def help_message(update, context):
    text = """
*You can use the following commands:*

_Register someone for a party_
/register

_Delete a registration_
/delete\_registration

_View your current registrations for upcoming parties_
/my\_registrations

_Subscribe to notifications of when events are happening_
/subcribe

_Unsubscribe from notifications about new events_
/unsubscribe

_Show this help message_
/help

_Show advanced command usages_
/help\_verbose
    """
    with DBSession() as session:
        if is_admin(update.effective_chat.id, session, context.bot, False):
            text += """

*Admin Commands*:

_Show all registrations for upcoming events_
/all\_registrations

_Add a date on which an event should take place_
/add\_date <date>

_Print the current guest list for a date to a CSV_
/get\_csv <date>
            """
    context.bot.send_message(update.effective_chat.id, text=text, parse_mode='Markdown')


def help_verbose(update, context):
    text = """
*You can use the following commands:*

_Register someone for a party_
/register

_Register someone directly (no buttons)_
/register <date> <vaccinated> <name>

_Delete a registration_
/delete\_registration

_Delete a registration directly (no buttons)_
/delete\_registration <date> <vaccinated> <name>

_View your current registrations for upcoming parties_
/my\_registrations

_View your registrations for specific dates_
/my\_registrations <date1> <date2> ...

_Subscribe to notifications of when events are happening_
/subcribe

_Unsubscribe from notifications about new events_
/unsubscribe

_Show this help message_
/help

_Show advanced command usages_
/help\_verbose
    """
    with DBSession() as session:
        if is_admin(update.effective_chat.id, session, context.bot, False):
            text += """

*Admin Commands*:

_Show all registrations for upcoming events_
/all\_registrations

_Show all registrations for specific dates_
/all\_registrations <date1> <date2> ...

_Add a date on which an event should take place_
/add\_date <date>

_Print the current guest list for a date to a CSV_
/get\_csv <date>
            """
    context.bot.send_message(update.effective_chat.id, text=text, parse_mode='Markdown')


def get_valid_capacity(input_string):
    args = input_string.split(' ')[1:]
    if len(args) != 1:
        text = 'Incorret number of arguments. Please enter exactly one number (e.g. "/set_capacity 20")'
        context.bot.send_message(chat_id=chat_id, text=text)
        return -1

    try:
        ret = int(args[0])
    except ValueError:
        text = "Argument doesn't appear to be a number. Please enter only a whole number"
        context.bot.send_message(chat_id=chat_id, text=text)
        return -1

    if ret < 0:
        text = "The capacity can't be lower than 0. Please use a valid number."
        context.bot.send_message(chat_id=chat_id, text=text)
        return -1

    return ret



def set_capacity(update, context):
    chat_id = update.effective_chat.id
    if is_group_chat(chat_id, context.bot):
        return

    with DBSession() as session:
        uid = update.effective_message.from_user.id
        chat_id = update.effective_chat.id
        if not is_admin(uid, session, context.bot):
            return

    tmp = get_valid_capacity(update.effective_message.text)
    if tmp == -1:
        return

    global CAPACITY
    CAPACITY = tmp
    text = f'Capacity updated to {CAPACITY}.'
    context.bot.send_message(chat_id=chat_id, text=text)


def set_max_capacity(update, context):
    chat_id = update.effective_chat.id
    if is_group_chat(chat_id, context.bot):
        return

    with DBSession() as session:
        uid = update.effective_message.from_user.id
        chat_id = update.effective_chat.id
        if not is_admin(uid, session, context.bot):
            return

    tmp = get_valid_capacity(update.effective_message.text)
    if tmp == -1:
        return

    global MAX_CAPACITY
    MAX_CAPACITY = tmp
    text = f'Maximum capacity updated to {MAX_CAPACITY}.'
    context.bot.send_message(chat_id=chat_id, text=text)


def get_capacity(update, context):
    chat_id = update.effective_chat.id
    if is_group_chat(chat_id, context.bot):
        return

    text = f'Currently the bar has {CAPACITY}/{MAX_CAPACITY} people inside.'
    context.bot.send_message(chat_id=chat_id, text=text)


if __name__ == "__main__":
    ROW_SIZE = 2
    REGISTER_DATE, REGISTER_NAME, REGISTER_VACCINATED, REGISTER_CONFIRM = range(4)
    DELETE_DATE, DELETE_NAME, DELETE_CONFIRM = range(3)
    with open("config.yaml") as config_file:
        config = yaml.safe_load(config_file)
        updater = Updater(token=config["token"], use_context=True)
        MAX_UNVAX = None
        MAX_PARTICIPANTS = None
        if config["strategy"] == "max_unvax":
            MAX_UNVAX = config["max_unvax"]
        elif config["strategy"] == "3g":
            MAX_PARTICIPANTS = config["max_participants"]
        else:
            print('Please ease either "max_unvax" or "3g" as a strategy')
            exit()
        PLAN_WEEKS_AHEAD = config["plan_weeks_ahead"]
        MAINTAINER = config["maintainer"]
        db_engine = create_engine(config["db_host"], pool_pre_ping=True)
        session_factory = sessionmaker(bind=db_engine)
        DBSession = scoped_session(session_factory)
        Base.metadata.create_all(db_engine)

    cancel_handler = CommandHandler('cancel', cancel)
    register_handler = CommandHandler('register', register)
    registration_conv_handler = ConversationHandler(
        entry_points=[register_handler],
        states={
            REGISTER_DATE: [CallbackQueryHandler(ask_for_name)],
            REGISTER_NAME: [MessageHandler(Filters.text & ~(Filters.command | Filters.regex('^Done$')),
                   ask_vaccinated if MAX_UNVAX else confirm)],
            REGISTER_VACCINATED: [CallbackQueryHandler(confirm)],
            REGISTER_CONFIRM: [CallbackQueryHandler(finish_registration)]
        },
        fallbacks=[cancel_handler, register_handler]
    )
    delete_handler = CommandHandler('delete_registration', delete_registration)
    delete_conv_handler = ConversationHandler(
        entry_points=[delete_handler],
        states={
            DELETE_DATE: [CallbackQueryHandler(delete_ask_for_name)],
            DELETE_NAME: [CallbackQueryHandler(delete_confirm)],
            DELETE_CONFIRM: [CallbackQueryHandler(finish_delete)]
        },
        fallbacks=[cancel_handler, delete_handler]
    )
    updater.dispatcher.add_handler(CommandHandler('start', start))
    MAX_CAPACITY = 25
    CAPACITY = 0
    #updater.dispatcher.add_handler(registration_conv_handler)
    #updater.dispatcher.add_handler(delete_conv_handler)
    #updater.dispatcher.add_handler(CommandHandler('my_registrations', my_registrations))
    #updater.dispatcher.add_handler(CommandHandler('help', help_message))
    #updater.dispatcher.add_handler(CommandHandler('help_verbose', help_verbose))
    #updater.dispatcher.add_handler(CommandHandler('subscribe', subscribe))
    #updater.dispatcher.add_handler(CommandHandler('unsubscribe', unsubscribe))
    updater.dispatcher.add_handler(CommandHandler('get_capacity', get_capacity))
    # Admin functions
    updater.dispatcher.add_handler(CommandHandler('set_capacity', set_capacity))
    updater.dispatcher.add_handler(CommandHandler('set_max_capacity', set_max_capacity))
    #updater.dispatcher.add_handler(CommandHandler("get_csv", get_csv))
    #updater.dispatcher.add_handler(CommandHandler('all_registrations', all_registrations))
    #updater.dispatcher.add_handler(CommandHandler("add_date", add_date))

    # schedule.every().day.at("15:00").do(remind_unregister)

    updater.start_polling()

    while True:
        schedule.run_pending()
        time.sleep(10)
